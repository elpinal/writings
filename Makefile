build:
	clj -X main/run

serve: build
	go run watch.go

watch:
	clj -X main/watch

.PHONY: build serve watch
