(ns main
  (:require [stasis.core :as stasis]
            hiccup.page
            java-time
            [clojure-watch.core :refer [start-watch]]))

(def target-dir "_build")

(def my-config
  {:domain "elpinal.gitlab.io"
   :head
   (fn [& {:keys [title]}]
     `([:meta {:charset "UTF-8"}]
       [:title ~title]
       [:meta {:content "width=device-width" :name "viewport"}]
       [:link {:href "/writings/css/style.css" :rel "stylesheet"}]))})

; Returns a map from Clojure file paths to their contents.
(defn get-articles [] (stasis/slurp-directory "articles" #"\.clj$"))

(def stylesheets (stasis/slurp-directory "css" #"\.css$"))

(defn render [[path content]]
  [(clojure.string/replace path #"\.clj$" "/") (-> content read-string eval)])

(defn format-month [m]
  (case m
    1 "Jan"
    2 "Feb"
    3 "Mar"
    4 "Apr"
    5 "May"
    6 "Jun"
    7 "Jul"
    8 "Aug"
    9 "Sep"
    10 "Oct"
    11 "Nov"
    12 "Dec"))

(defn tformat [y m d]
  (let [date (java-time/offset-date-time y m d)]
    [:time {:datetime (java-time/format :iso-offset-date-time date)}
     (str (format-month m) " " (java-time/format "d, yyyy" date))]))

(defn index [context]
  (hiccup.page/html5
   [:head ((:head context) :title "Writings by El Pin Al")]
   [:body
    [:h1 "Writings"]

    [:section
     [:h1 "2022"]

     [:ul
      [:li
       [:a {:href "/writings/agda/"} "Agdaに入門した"]
       [:br]
       (tformat 2022 1 8)]]]

    [:section
     [:h1 "2020"]

     [:ul
      [:li
       [:a {:href "/writings/withtype/"} "Standard MLのwithtypeの挙動"]
       [:br]
       (tformat 2020 12 2)]]]

    [:section
     [:h1 "2019"]

     [:ul
      [:li
       [:a {:href "/writings/alice/"} "Alice MLのモジュールシステム"]
       [:br]
       (tformat 2019 12 20)]

      [:li
       [:a {:href "/writings/ocaml-undecidable-typechecking/"} "OCamlの型検査は決定不能"]
       [:br]
       (tformat 2019 8 18)]

      [:li
       [:a {:href "/writings/recmod/"} "再帰モジュールとは"]
       [:br]
       (tformat 2019 3 23)]]]]))

(defn get-pages []
  (merge
   {"/index.html" index}
   (into {} (map (fn [[path content]] [(str "/css" path) content]) stylesheets))
   (into {} (map render (get-articles)))))


(defn export []
  (stasis/empty-directory! target-dir)
  (stasis/export-pages (get-pages) target-dir my-config))

(defn run [args]
  (println "exporting...")
  (export)
  (println "done."))

(defn watch [args]
  (export)
  (start-watch
   [{:path "articles"
     :event-types [:create :modify :delete]
     :bootstrap (fn [path] (println "starting to watch" path))
     :callback (fn [event filename] (export))
     :options {:recursive true}}])
  (while true))
