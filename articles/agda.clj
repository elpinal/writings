(fn [ctx]
  (let [title "Agdaに入門した"
        date (java-time/offset-date-time 2022 1 8)]
    (hiccup.page/html5
     [:head
      ((:head ctx) :title title)]
     [:body
      [:h1 title]
      [:time {:datetime (java-time/format :iso-offset-date-time date)}
       (java-time/format "yyyy/MM/dd" date)]

      [:p
       [:a {:href "https://agda.readthedocs.io/en/v2.6.2.1/getting-started/what-is-agda.html"} "Agda"]
       "のインストールは、"
       [:a {:href "https://agda.readthedocs.io/en/v2.6.2.1/getting-started/installation.html#os-x"} "ドキュメントの指示"]
       "に従い、"
       [:code "brew install agda"]
       "でやりました。"
       "Agdaのバージョンは2.6.2.1で、standard libraryのバージョンは1.7.1でした。"

       "環境はM1 Mac (Big Sur)です。"

       [:code "agda-mode setup"]
       "が上手く動かなかったので、手動で" [:code "~/.emacs.d/init.el"] "を編集しました。"]

      [:pre [:code "(load-file (let ((coding-system-for-read 'utf-8))
                (shell-command-to-string \"agda-mode locate\")))"]]

      [:p
       "exec-path-from-shellも念のためemacsにインストールし、"
       [:code "(exec-path-from-shell-initialize)"]
       "を"
       [:code "~/.emacs.d/init.el"]
       "に追記しています。"]

      [:p
       "standard libraryの設定もする必要があります。"
       "ドキュメントには"
       [:code "/usr/local/lib/agda/"]
       "にstandard libraryがあると書いてありますが、私の環境では"
       [:code "/opt/homebrew/lib/agda/"]
       "にありました。"
       [:code "$(brew --prefix)/lib/agda/"]
       "の内容を見るのが確実です。"

       "作業としては、"
       [:code "~/.agda/libraries"]
       "と"
       [:code "~/.agda/defaults"]
       "に適切な内容を記入するだけです。"]

      [:p
       "Terminal.app内のemacsのagda-modeの表示が崩れていたので、試行錯誤の結果、"]

      [:ul
       [:li "Terminal.appの「Unicode 東アジアA(曖昧)の文字幅をW(広)にする」をオフ(これは多分デフォルト)"]
       [:li "emacsの" [:code "M-x set-language-environment"] "でEnglishを選択する"]
       [:li "Terminal.appのフォントをMenloにする(これが必要かどうかは分かりません)"]]

      [:p "で解決しました。"]

      [:section
       [:h1 "入門"]
       [:p
        "公式のチュートリアル（"
        [:a {:href "https://agda.readthedocs.io/en/v2.6.2.1/getting-started/hello-world.html"} "‘Hello world’ in Agda"]
        "と"
        [:a {:href "https://agda.readthedocs.io/en/v2.6.2.1/getting-started/a-taste-of-agda.html"} "A Taste of Agda"]
        "）と、"
        [:a {:href "https://plfa.github.io"} "Programming Language Foundations in Agda"]
        "のPart 1をやりました。"

        "Programming Language Foundations in Agdaは、Part 1だけでも数日はかかります。"

        "これだけやれば、自力である程度は書けるようになります。"]]

      [:section
       [:h1 "ライブラリのドキュメント"]
       [:p
        "standard libraryのドキュメントは、"
        [:a {:href "https://agda.github.io/agda-stdlib/README.html"} "ここ"]
        "から見ることが可能ですが、リリースされたバージョンではなくmasterのドキュメントなので、"
        "インストールされているバージョンと内容が異なる場合があります。"
        "たとえば、standard library 1.7.1には"
        [:code "Data.Fin.raise"]
        "しかありませんが、"
        "masterには"
        [:code "Data.Fin._↑ˡ_"] "と" [:code "Data.Fin._↑ʳ_"] "があります。"

        "したがって、HTMLで見るかわりに、emacsで"
        [:code "$(brew --prefix)/lib/agda/src/"]
        "以下を開けば、インストールされているバージョンのドキュメントを見ることができます。"
        "emacsで開くことで、"
        [:code "C-c C-l"]
        "でシンタックスハイライトが付き、"
        [:code "M-."]
        "で定義にジャンプすることができるので、これで十分だと気付きました。"
        "（Unicode文字を使った検索も、webブラウザよりemacsの方がやりやすいという利点があります。）"]
]

      [:section
       [:h1 "Heterogeneous Equality"]

       [:p
        [:code "Vec"]
        "について、"
        [:code "[]"]
        "が"
        [:code "_++_"]
        "の右単位元であることを証明したいとき、"
        "そのような命題は" [:code "subst"] "を用いて"]

       [:pre [:code "prop : Set₁
prop = forall {A : Set} {n : ℕ} (xs : Vec A n) ->
       subst (Vec A) (+-identityʳ n) (xs ++ []) ≡ xs"]]

       [:p
        "と表現することができます。"
        [:code "subst"] "を使う理由は、"
        [:code "xs ++ []"] "の型は" [:code "Vec A (n + zero)"] "であって" [:code "Vec A n"]
        "ではないからです。"
        "このような状況では、"
        [:code "Relation.Binary.HeterogeneousEquality._≅_"]
        "を使うと、"]

       [:pre [:code "prop : Set₁
prop = forall {A : Set} {n : ℕ} (xs : Vec A n) -> xs ++ [] ≅ xs"]]

       [:p
        "と表現することができます。"
        [:code "subst P eq p"]
        "は第2引数が" [:code "refl"] "であると分かったときにしか"
        [:code "p"]
        "にならないので、"
        "その点でheterogeneous equalityの方が便利なときもある気がします。"]]

      [:section
       [:h1 "Uniqueness of Identity Proofs (UIP)"]

       [:p
        "Agdaには、"
        [:code "--with-K"]
        "や"
        [:code "--without-K"]
        "というオプションがあり、これらによって"
        [:code "K"]
        "と呼ばれる規則が成り立つかどうかが決まります。"
        "デフォルトでは" [:code "--with-K"] "が有効になっているので、"]

       [:pre [:code "K : {A : Set} {x : A} (P : x ≡ x → Set) →
    P refl → (x≡x : x ≡ x) → P x≡x
K P p refl = p"]]

       [:p
        "を証明することができます。"
        "これは、述語" [:code "P"] "が" [:code "refl : x ≡ x"] "について成り立つならば、"
        "任意の" [:code "x≡x : x ≡ x"] "について" [:code "P"] "が成り立つことを意味します。"]

       [:p
        [:code "--without-K"] "を有効にすると、この" [:code "K"] "は型検査に通らなくなります。"
        "そのかわり、"]

       [:pre [:code "J : {A : Set} (P : (x y : A) → x ≡ y → Set) →
    ((x : A) → P x x refl) → (x y : A) (x≡y : x ≡ y) → P x y x≡y
J P p x .x refl = p x"]]

       [:p
        "は証明することができます。（この" [:code "J"] "は、" [:code "--with-K"] "でも証明できます。）"
        "これは、任意の" [:code "x : A"] "について" [:code "P x x (refl : x ≡ x)"]
        "が成り立つならば、"
        "任意の" [:code "x : A, y : A, x≡y : x ≡ y"] "について"
        [:code "P x y x≡y"] "が成り立つことを意味します。"]

       [:p
        "詳しくは"
        [:a {:href "https://agda.readthedocs.io/en/v2.6.2.1/language/without-k.html"} "Without K — Agda 2.6.2.1 documentation"]
        "を見てください。"]

       [:p
        [:code "--with-K"] "が有効であるときは、"
        [:a {:href "https://agda.github.io/agda-stdlib/Axiom.UniquenessOfIdentityProofs.WithK.html"} "Uniqueness of Identity Proofs (UIP)"]
        "が任意の型について成り立ちます。"
        "UIPは、equality (" [:code "≡"] ")の証明が、（存在するならば）一意であるという主張です。"]]

      [:section
       [:h1 "Cubical Agda"]

       [:p
        "Homotopy type theory (HoTT)について聞いたことのある人も多いと思いますが、"
        "Cubical type theoryは、より良い計算的性質（canonicityやnormalization）を持つHoTTと言えると思います。"
        "特徴的な点は、"
        [:a {:href "https://github.com/agda/cubical/blob/ce8724530361f18e237401ee89d3d26b54d4cfba/Cubical/Foundations/Univalence.agda#L36"} "univalence"]
        "が公理ではなく定理であることでしょう。"

        "Cubical type theoryには色々な種類がありますが、特にDe Morgan Cubical type theoryと呼ばれる型理論が、"
        [:a {:href "https://agda.readthedocs.io/en/v2.6.2.1/language/cubical.html"} "Cubical Agda"]
        "として実装されています。"

        "Cubical Agdaとしての最低限の機能は"
        [:code "--cubical"] "オプションを使うことで利用できますが、"
        "実用上は"
        [:a {:href "https://github.com/agda/cubical"} "cubical"]
        " libraryを使うことが多いでしょう。"]

       [:p
        "cubical libraryのインストールは、"
        [:code "git clone https://github.com/agda/cubical && cd cubical && make"]
        "を実行した後、"
        [:code "cubical.agda-lib"]
        "へのパスを"
        [:code "~/.agda/libraries"]
        "に追記し、"
        [:code "cubical"]
        "という文字列を"
        [:code "~/.agda/defaults"]
        "に追記すればよかった気がします。"]

       [:p
        "Cubical Agdaはinterval typeと呼ばれる型" [:code "I"] "を導入します。"
        "interval typeには2つの定数" [:code "i0 : I"] "と" [:code "i1 : I"]
        "があります。"
        "注意すべき点は、interval typeを持つ項は" [:code "i0, i1"]
        "だけでなく、その2つの「間」に無数の「点」が存在すると考えられることです。"
        "interval typeの項はDe Morgan代数を成します。"
        "De Morgan代数は、Boole代数（古典論理のモデル）から排中律と無矛盾律を除いたものです。"]

       [:p
        "Cubical Agdaにおいて、equalityは、"
        [:code "Relation.Binary.PropositionalEquality._≡_"]
        "ではなく、path typeと呼ばれるものを使います。"
        "型" [:code "A"] "と" [:code "x : A, y : A"] "について、path type" [:code "x ≡ y"] "は、"
        "関数" [:code "I -> A"] "のように扱われます。"]

       [:p
        "path type" [:code "x ≡ y"] "はdependent path type "
        [:code "PathP (λ _ -> A) x y"] "の特別な場合です。"
        [:code "PathP"] "を使うと、heterogeneous equalityを"
        [:a {:href "https://github.com/agda/cubical/blob/ce8724530361f18e237401ee89d3d26b54d4cfba/Cubical/Data/Vec/Properties.agda#L21"} "より上手く扱える"]
        "ようになります。"]

       [:p
        "Cubical Agdaでは" [:code "--without-K"] "と同じように、UIPが一般には成り立ちません。"
        "UIPが成り立つ型に関しては、"
        [:code "isProp"] "や" [:code "isSet"] "が役立ちます。"
        "たとえば、"
        [:code "m : ℕ, n : ℕ"]
        "について、"
        [:code "m ≡ n"]
        "の証明が2つある場合、"
        [:code "Cubical.Data.Nat.isSetℕ"]
        "を使うことで、それらが同じ(equal)であることを証明できます。"]

       [:p
        "Cubical Agdaの文献としては、"
        [:a {:href "https://doi.org/10.1145/3434293"} "Internalizing representation independence with univalence"]
        "などを読むと良いです。"
        "（本稿ではHITについて触れませんでしたが、この論文はHITを上手く利用しています。）"]]

      [:section
       [:h1 "さいごに"]

       [:p
        "この文章について何か誤り等があれば"
        [:a {:href "https://twitter.com/elpin1al"} "@elpin1al"] "まで"
        "ご連絡ください。"]]])))
