(fn [ctx]
  (let [title "OCamlの型検査は決定不能"
        sml "Standard ML"
        ross1999 "https://sympa.inria.fr/sympa/arc/caml-list/1999-07/msg00027.html"
        hl1994 "https://www.cs.cmu.edu/~rwh/papers/sharing/popl94.pdf"
        ross2018 "https://people.mpi-sws.org/~rossberg/papers/Rossberg%20-%201ML%20--%20Core%20and%20modules%20united%20[JFP].pdf"
        ocaml-transparent "https://blog.janestreet.com/a-look-at-ocaml-4.08/#strengthening-the-module-system"
        emph (fn [x] [:em x])]
    (hiccup.page/html5
     [:head
      ((:head ctx) :title title)]
     [:body
      [:h1 title]

      [:p
       "1990年代から(少なくとも一部の人に)知られていることですが、OCamlの型検査は決定不能です。この文書では、ML系モジュールシステムにおけるシグネチャマッチングの仕組みを説明した後、OCamlの型検査器がどのように無限ループに陥るかを確認します。"]

      [:section
       [:h1 "シグネチャマッチング"]

       [:p
        "ML系言語において、シグネチャは、モジュールのインターフェイスを表すために用いられます。あるモジュールが何らかのシグネチャに適合していることを保証するためにsignature ascriptionという機構があります。"]

       [:p
        sml "では、" [:code "M : S"] "や" [:code "M :> S"] "という風に書くと、モジュール" [:code "M"] "がシグネチャ" [:code "S"] "に適合していることを保証できます。"
        [:code "M : S"] "はtransparent signature ascriptionと呼ばれ、"
        [:code "M :> S"] "はopaque signature ascriptionと呼ばれます。"]

       [:p
        "シグネチャ" [:code "S"] "の" (emph "instance") "とは、"
        [:code "S"] "の抽象型コンポーネントを何らかの型で置換したものを指します。"
        "そして、このときの置換を" (emph "realization") "と呼びます。"
        "たとえば、" [:code "S = sig type t; val f : t -> t end"] "であるとき、次の全てのシグネチャが" [:code "S"] "のinstanceとなります。"]

       [:ul
        [:li [:code "sig type t = int; val f : t -> t end"]]
        [:li [:code "sig type t = string -> bool; val f : t -> t end"]]
        [:li [:code "sig type t = string -> bool; val f : t -> string -> bool end"]]]

       [:p
        "シグネチャマッチングには2つの側面があります。"
        "1つは、"
        [:code "S"] "の任意のinstanceが、" [:code "S"] "にマッチすることを許し、"
        "もう1つは、よりコンポーネントが多いシグネチャが、よりコンポーネントの少ないシグネチャにマッチすることを許します。"]

       [:p
        [:code "M : S"] "と" [:code "M :> S"] "の違いは、"
        [:code "M : S"] "はrealizationを明らかにしたままのシグネチャが全体のシグネチャになるのに対し、"
        [:code "M :> S"] "はrealizationを隠してしまうことです。"]]

      [:section
       [:h1 "OCamlの型検査"]

       [:p
        "OCamlにはabstract signature specification (OCamlの用語ではabstract module type specification)という機能が" [:a {:href "https://caml.inria.fr/pub/docs/manual-ocaml-4.08/modtypes.html#sec209"} "あります"] "。これは" (emph "任意") "のシグネチャにマッチします。"]

       [:p "次のように書くと、"]
       [:pre
        [:code
         "module type S = sig
  module type T
end

module M : S = struct
  module type T = sig
    type t = int
  end
end"]]
       [:p "「任意のシグネチャ" [:code "T"] "」をコンポーネントに持つシグネチャが"
        [:code "S"] "にマッチします。"
        "ちなみにOCamlのモジュールレベルの" [:code ":"] "はopaque signature ascriptionですので、" sml "の" [:code ":>"] "に対応します。"
        "OCamlにもtransparent signature ascriptionが(" [:code "<:"] "という記号で)入る予定はあるようですが、OCaml 4.08には" [:a {:href ocaml-transparent} "入りませんでした"] "。"]

       [:p
        "さて次のコードは、1999年にRossbergがcaml-listに投稿したもの"
        [:a {:href "#rossberg1999"} "[2]"]
        "を簡略化したものです。"]

       [:pre
        [:code
         "module type I = sig
  module type A
  module F :
    sig
      module type A = A
      module F : A -> sig end
    end -> sig end
end

module type J = sig
  module type A = I
  module F : I -> sig end
end

module Loop(X : J) = (X : I)
"]]
       [:p "最後の" [:code "Loop"] "ファンクタは" [:code "J <: I"] "かどうかを試すためにあります。以下で実際に" [:code "J <: I"] "を計算します。"]
       [:pre
        [:code
         "J <: I

-- まず最初のコンポーネントAのinstanceがIだと分かる --

J <: sig
  module type A = I
  module F :
    sig
      module type A = A
      module F : A -> sig end
    end -> sig end
end

-- 最初のコンポーネントAを削除しつつ、残りの部分のAをIで置換する --

sig
  module F : I -> sig end
end
<:
sig
  module F :
    sig
      module type A = I
      module F : A -> sig end
    end -> sig end
end

-- congruence --

I -> sig end
<:
sig
  module type A = I
  module F : A -> sig end
end -> sig end

-- contravariant --

sig
  module type A = I
  module F : A -> sig end
end
<:
I

sig
  module type A = I
  module F : I -> sig end
end
<:
I

J <: I"]]

       [:p
        "というように" [:code "J <: I"] "にまた戻ってしまいます。このコードを実際にOCamlで型検査すると無限ループに陥ります。"]
       ]

      [:section
       [:h1 "原因"]

       [:p
        [:code "¬A := (A -> sig end)"] "と定義すると、ファンクタ引数の反変性より"
        [:code "¬A <: ¬B ⇄ B <: A"] "が成り立ちます。"
        "これにより部分型関係の左辺と右辺を入れ替えられます。"]

       [:p
        "Harper & Lillibridge "
        [:a {:href "#hl1994"} "[1]"]
        "の言語の型検査は決定不能です。この結果は次の3つの条件を満たす任意の言語にも適用されます。"]

       [:ol
        [:li "反変関数を持つ。"]
        [:li "transparent typeとopaque typeの両方を持つ。"]
        [:li "任意のtransparent typeがopaque typeの部分型になることを許す。"]]

       [:p
        "要するに、シグネチャ上を抽象化できるとき、シグネチャマッチングは決定不能になります。これは(true) first-class moduleの型検査が決定不能になる原因です。"
        "そして、abstract signatureの存在するOCamlでも、同じ現象がモジュールレベルで発生します。"]

       [:p
        "Moscow ML (2.00, 2000年)やOCaml (3.12.0, 2010年)がサポートしているfirst-class packaged moduleとは違って、コア言語とモジュール言語の区別を無くした本当の意味でのfirst-class moduleを実現した言語として、1ML " [:a {:href "#rossberg2018"} "[3]"] "が存在します。"
        "1MLは「opaque typeの部分型になれるtransparent typeを、抽象型や型パラメータを含まないもの(いわゆるsmall type)に制限する」という方法で3つ目の条件を回避することによって静的に決定可能な型検査を達成しています。"]]

      [:section
       [:h1 "参考文献"]

       [:ol {:class "square"}
        [:li {:id "hl1994"} "Robert Harper and Mark Lillibridge. " [:a {:href hl1994} "A type-theoretic approach to higher-order modules with sharing"] ". 1994."]
        [:li {:id "rossberg1999"} "Andreas Rossberg. " [:a {:href ross1999} "Undecidability of OCaml type checking"] ". 1999."]
        [:li {:id "rossberg2018"} "Andreas Rossberg. " [:a {:href ross2018} "1ML — Core and modules united"] ". 2018."]]]
      ])))
