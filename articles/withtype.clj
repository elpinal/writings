(fn [ctx]
  (let [title "Standard MLのwithtypeの挙動"
        tau "τ"
        sml "Standard ML"
        sml97 "http://sml-family.org/sml97-defn.pdf"
        hl1994 "https://www.cs.cmu.edu/~rwh/papers/sharing/popl94.pdf"
        type-and [:code "type ... and ..."]
        ocaml-transparent "https://blog.janestreet.com/a-look-at-ocaml-4.08/#strengthening-the-module-system"
        emph (fn [x] [:em x])]
    (hiccup.page/html5
     [:head
      ((:head ctx) :title title)]
     [:body
      [:h1 title]

      [:section
       [:p
        sml "には" [:code "type ... and ..."] "という構文があり、複数の型シノニムを"
        [:em "同時に"] "定義することができます。"
        "たとえば、次のコードは型シノニム" [:code "a"] "と" [:code "b"] "を定義します。"]

       [:pre [:code "type a = int -> bool
and b = real * int"]]

       [:p
        "この例では、" [:code "int -> bool"] "と" [:code "real * int"] "を「解釈」"
        "した" [:em "後に"] [:code "a"] "と" [:code "b"] "を束縛します。"]

       [:p
        type-and "でしか出来ないことは「一時的な型識別子を導入することなく、複数の型識別子を入れ替える」ことです。"
        "次のコード例では、コンストラクタ" [:code "A"] "を持つdatatype " [:code "a"]
        "と、コンストラクタ" [:code "B"] "を持つdatatype " [:code "b"] "を定義した後に、"
        "型識別子" [:code "a"] "と" [:code "b"] "を入れ替えています。"
        "その結果、コンストラクタ" [:code "A"] "は型" [:code "b"] "を、"
        "コンストラクタ" [:code "B"] "は型" [:code "a"] "を持つようになります。"]

       [:pre [:code "datatype a = A
datatype b = B

val _ : a = A
val _ : b = B

type a = b
and b = a

val _ : b = A
val _ : a = B
"]]

       [:p
        type-and "が無ければ、この例は次のように書き直されるでしょう。"]

       [:pre [:code "datatype a = A
datatype b = B

local type a' = a in
  type a = b
  type b = a'
end
"]]

       [:p
        "このように、" [:code "local"] "を使った方法では一時的な型識別子"
        "を導入する必要があり、また、その識別子は"
        [:code "a"] "や" [:code "b"] "とは異なるもの(ここでは" [:code "a'"]
        ")を選択しなければなりません。"]

       [:p
        "しかしながら、このような型識別子の入れ替えは、あまり便利だとは思えません。"
        type-and "が真に活躍するのは「複数の型定義の" [:em "順番"] "が重要ではないことを表す」ときでしょう。"
        "たとえば、次のコードは型シノニム" [:code "window"] "と" [:code "cursor"]
        "を定義しています。"]

       [:pre [:code "type window = {
  height : int,
  width : int
}

and cursor = {
  offset : int,
  active : bool
}
"]]

       [:p
        "このとき、" [:code "window"] "と" [:code "cursor"] "の順番を入れ替えるのは簡単です。"
        "なぜなら、" type-and "が「" [:code "window"] "と" [:code "cursor"] "の定義の順番が重要ではない」ことを示しているからです。"]

       [:p
        "一方、もしこの例が連続した" [:code "type"] "宣言で構成されていた場合、"
        "つまり次の例のとき、"]

       [:pre [:code "type window = {
  height : int,
  width : int
}

type cursor = {
  offset : int,
  active : bool
}
"]]

       [:p
        [:code "window"] "の定義と" [:code "cursor"] "の定義を入れ替えるためには、"
        [:code "window"] "の定義が" [:code "cursor"] "を含まないことと、"
        [:code "cursor"] "の定義が" [:code "window"] "を含まないことを確認する必要があります。"]]

      [:section
       [:h1 [:code "withtype"]]

       [:p
        sml "には" [:code "withtype"] "という機能があります。"
        "たとえば、次のように書かれます。"]

       [:pre [:code "datatype 'a c = C of bool d -> 'a
withtype 'b d = ('b * int) c
"]]

       [:p
        "このコードは、次のようなコードの糖衣構文です。"]

       
       [:pre [:code "datatype 'a c = C of (bool * int) c -> 'a
type 'b d = ('b * int) c
"]]]

      [:section
       [:h1 [:code "withtype ... and ..."] "について"]
       
       [:section
        [:h1 "Sample code"]

        [:pre [:code "datatype s = C of t * u
withtype t = s -> s
and u = t * bool
"]]]

       [:section
        [:h1 "SML/NJ"]

        [:p "SML/NJでは通る。"]
        ]

       [:section
        [:h1 "Moscow ML"]

        [:p "Moscow MLでは通らない。"]
        [:pre [:code "! and u = t * bool
!         ^
! Unbound type constructor: t
"]]
        [:p [:code "s"] "の定義から" [:code "* u"] "を取り除いても同じエラーが出ることから、"
         "脱糖後の" [:code "type t = s -> s and u = t * bool"] "がDefinition通りに"
         "エラーになってることが分かります。"]
        ]

       [:section
        [:h1 "MLton"]

        [:p "MLtonでは通らない。"]
        [:pre [:code "Undefined type t."]]]

       [:section
        [:h1 "MLKit"]

        [:p "MLKitでも通らない。"]
        [:pre [:code "  and u = t * bool
          ^
unbound type constructor t.
"]]]

       [:section
        [:h1 "Alice ML"]

        [:p "Alice MLでも通らない。"]

        [:pre [:code "unknown type `t'"]]]

       [:section
        [:h1 "HaMLet"]

        [:p "HaMLetでも通らない。"]

        [:pre [:code "unknown type t"]]]

       [:section
        [:h1 "Poly/ML"]

        [:p "Poly/MLでも通らない。"]

        [:pre [:code "Type constructor (t) has not been declared"]]]

       [:section
        [:h1 "SML#"]

        [:p
         [:a {:href "https://twitter.com/blackenedgold/status/1334173340764835843"}
          "κeenさんによると"]
         "、SML#でもDefinition通り、" [:code "unbound type constructor or type alias: t"]
         "というエラーがでるようです。(情報提供感謝)"]]

       [:section
        [:h1 "他の処理系"]

        [:p
         "TILTはインストールに失敗したので確認できませんでした。"
         ]]]

      [:section
       [:h1 "付録: シグネチャ内でのwithtype"]

       [:p
        "Definitionでは、" [:code "withtype"] "は" [:code "let"] "やストラクチャ内では"
        "使うことが出来るものの、"
        "シグネチャ内では使うことが出来ません。"]

       [:p "SML/NJ, Poly/ML, Moscow ML, Alice MLではシグネチャ内でwithtypeを使うことができます。"]]

      
      ])))
