(fn [ctx]
  (let [title "再帰モジュールとは"
        quote (fn [s] (str "“" s "”"))
        code (fn [s] [:pre [:code s]])
        paper {:title "What is a recursive module?"
               :authors ["Karl Crary" "Robert Harper" "Sidd Puri"]
               :year 1999
               :location "In ACM SIGPLAN Conference on Programming Language Design and Implementation (PLDI), pages 50–63, Atlanta, Georgia"
               :url "http://www.cs.cmu.edu/~crary/papers/1999/recmod/recmod.ps.gz"}
        bib (fn [entry]
              [:section
               [:h1 [:a {:href (:url entry)} (:title entry)]]
               [:p
                (interpose ", " (:authors entry))
                "."
                [:br]
                (:location entry) ", " (str (:year entry) ".")]])]
    (hiccup.page/html5
     [:head
      ((:head ctx) :title title)]

     [:body
      [:h1 title]

      [:section
       [:p
        "ML系の言語などが持つ階層的なモジュールシステムは大きなソフトウェアを分割して構成するための効果的なツールです。"
        "しかし、そのような強い非循環性を要求するモジュールシステムを利用する際は、"
        "相互再帰する型や項を1つのモジュールに押し込めなければなりません。"]

       [:p
        [:em "再帰モジュール(recursive modules)"] "はそのような問題を解決するための機構であり、"
        "自己言及可能なモジュールを指します。Standard ML風の構文では次のように書かれます。"]

       (code
        "signature LIST =
  sig
    type t
    val nil : t
    val cons : int * t -> t
  end

structure rec List :> LIST =
  struct
    datatype t = NIL | CONS of int * List.t
    val nil = NIL
    fun cons (n, l) =
      case l of
        NIL          => CONS(n, List.nil)
      | CONS(hd, tl) => CONS(n, List.cons (hd, tl))
  end")

       [:p
        "このプログラムは整数のリストの、再帰モジュールによる実装例です。"
        [:code "LIST"] "シグネチャは何の変哲もないシグネチャですが、" [:code "List"] "ストラクチャの宣言には" [:code "rec"] "という記号が付いています。この" [:code "rec"] "は" [:code "List"] "が再帰モジュールとして定義されることを意味します。"
        "再帰モジュールの内部では、"
        [:code "List.t"]
        "や"
        [:code "List.nil"]
        "のように、自身への言及が可能になります。"]

       [:p
        [:code "List"]
        "ストラクチャはプログラムの残りの部分と、"
        [:code "List"]
        "自身からopaqueに扱われます。言い換えれば、"
        [:code "LIST"] "シグネチャが"
        [:code "List"] "ストラクチャに関する全ての情報を表します。"]

       [:p
        "この定義では、再帰型も再帰関数も使わずに、再帰モジュールだけで再帰構造を表現しています。"]
       [:p
        "ちなみに、"
        [:code "cons"]
        "の定義が"
        [:code "fun cons (n, l) = CONS(n, l)"]
        "となっていないのは、現時点ではそのように出来ないからです。この件については"
        [:a {:href "#dvp"} "double vision problemの節"]
        "で話します。"]]

      [:section
       [:h1 "再帰モジュールは既に存在する"]

       [:p
        "先程の例は、再帰モジュールが無くても次のように書けます(少し冗長ですが、比較のためです)。"]

       (code
        "signature LIST = ...

structure List :> LIST = struct
  datatype t = NIL | CONS of int * t
  val nil = NIL
  fun cons (n, l) =
    case l of
      NIL          => CONS(n, nil)
    | CONS(hd, tl) => CONS(n, cons(hd, tl))
end")
       [:p
        "この事が示唆するように、再帰モジュールはストラクチャと再帰型、再帰関数で表現できます。(Phase separationを知っている人のために言うと、これは高階モジュールがストラクチャと型レベル関数、高階関数で表現可能なことと似ています。)"]]

      [:section
       [:h1 {:id "dvp"} "Double vision problem"]

       [:p
        "これで話が終われば良いのですが、型生成性と再帰モジュールが絡むと話が複雑になります。上のリストの例では"
        [:code "cons"]
        "がO(n)になってしまいます。"
        [:code "List.t"]
        "がopaqueである以上、"
        [:code "List.t"]
        "と"
        [:code "t"]
        "が同じ型であることが分からないのです。本来1つであるはずの型が2つの異なる型に見えてしまうことから、この問題を"
        [:em "double vision problem"]
        "と呼びます。"]

       [:p
        "この問題の解決策として、シグネチャで"
        [:code "List.t = t"]
        "という事実を捉えることにします。このために"
        [:em "再帰的依存シグネチャ(recursively dependent signature)"]
        "というシグネチャを用います。以下に再帰的依存シグネチャを利用した"
        [:code "List"]
        "ストラクチャの例を載せます。"]

       (code
        "structure rec List :>
  sig
    datatype t = NIL | CONS of int * List.t
    ...
  end =
  struct
    datatype t = NIL | CONS of int * List.t
    ...
    fun cons (n, l) = CONS(n, l)
  end
")

       [:p
        "再帰的に定義されるモジュールのシグネチャが、そのモジュール自身に依存していることから、そのシグネチャは再帰的依存シグネチャと呼ばれます。"
        "このように再帰的依存シグネチャを使うことで"
        [:code "List.t"]
        "が"
        [:code "t"]
        "と一致することを保証できます。"]]

      [:section
       [:h1 "展望"]

       [:p
        "ここまで話した内容は基本的に"
        (str (:year paper))
        "年の"
        (quote (:title paper))
        "に基づくものです。この後、2000年から2010年くらいの間に、型変数の生成と定義を分離するようになったり、さらにそこから"
        [:a {:href "http://gallium.inria.fr/~remy/modules/Montagu-Remy@popl09:fzip.pdf"} "open existential types"]
        "に繋がっていったりします。特にopen existential typesは面白いので読んでみてください(再帰モジュールそのものの話ではありませんが)。"]

       [:p
        "Double vision problemは、元々"
        (:title paper)
        "ではtrouble with opacityと呼ばれていた問題ですが、最近ではdouble vision problemと呼ばれる傾向にあると思います。"]

       [:p
        (:title paper)
        "では、高階カインドにおける同値再帰型コンストラクタの同値性を検査する必要があるのですが、その同値性は決定的だと知られているわけではなく(not known to be decidable)、決定的な型検査は実装できないと見てよいと思います。もっと実用的な再帰モジュールシステムについて知りたいなら、それ以降の論文を読む必要があるでしょう。"
        [:a {:href "https://github.com/elpinal/modules"} "モジュールシステムのbibliography"]
        "から"
        "recursive"
        "で検索してみるとよいと思います。"]]

      [:section
       [:h1 "参考文献"]

       (bib paper)]])))
