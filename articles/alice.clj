(fn [ctx]
  (let [title "Alice MLのモジュールシステム"
        sml "Standard ML"
        sml97 "http://sml-family.org/sml97-defn.pdf"
        hl1994 "https://www.cs.cmu.edu/~rwh/papers/sharing/popl94.pdf"
        ross2018 "https://people.mpi-sws.org/~rossberg/papers/Rossberg%20-%201ML%20--%20Core%20and%20modules%20united%20[JFP].pdf"
        ocaml-transparent "https://blog.janestreet.com/a-look-at-ocaml-4.08/#strengthening-the-module-system"
        emph (fn [x] [:em x])]
    (hiccup.page/html5
     [:head
      ((:head ctx) :title title)]
     [:body
      [:h1 title]

      [:p
       [:a {:href "https://www.ps.uni-saarland.de/alice/index.html"} "Alice ML"]
       "とは、Standard MLをベースにした言語であり、平行性や制約プログラミングなどをサポートしているのが特徴です。"
       "今回はAlice MLのモジュールシステムを取り上げます。"]

      [:p
       "なお、この文書でStandard ML 97と言うときは、The Definition of Standard ML (Revised) " [:a {:href "#sml97"} "[1]"] "で定義された言語を指します。"]

      [:p
       "対象読者: ML系言語に慣れていると読みやすいと思います。"]

      [:section
       [:h1 "MLモジュール速習ガイド"]

       [:p
        "MLのモジュールシステムの基本要素はストラクチャ、ファンクタ、シグネチャの3つです。"
        "その内ストラクチャとファンクタをまとめてモジュールと呼びます。"
        "シグネチャはモジュールの型です。"]]

      [:section
       [:h1 "局所的なモジュール定義"]

       [:p
        "Standard ML 97とは違って、Alice MLでは" [:code "let"] "式内でモジュールやシグネチャを定義することができます:"                    
        ]

       [:pre [:code "fun f x =
let
  structure M = struct
    val v = 1
  end
in
  M.v + x
end"]]

       [:p
        "これは、ファンクタ適用や、後述するパッケージの利用に役立ちます。"]]

      [:section
       [:h1 "高階ファンクタ"]

       [:p
        "MLのモジュールシステムの文脈では、ファンクタ (functor)といえばモジュール上の関数のことです。"
        "ファンクタを引数に取ったり、結果として返すようなファンクタを" [:em "高階ファンクタ (higher-order functor)"] "と呼びます。"
        "一方、ストラクチャを引数に取り、ストラクチャを返すファンクタを" [:em "1階ファンクタ (first-order functor)"] "と呼びます。"
        "Standard ML 97では1階ファンクタのみがサポートされていますが、Alice MLでは、さらに高階ファンクタもサポートされています。"
        "次のコードでは、ファンクタ" [:code "F"] "は「シグネチャ" [:code "S -> T"]
        "を持つファンクタを受け取り、シグネチャ" [:code "S -> T"]
        "を持つファンクタを返す」ファンクタです。"]

       [:pre [:code "signature S = sig
  type t
end

signature T = sig
  type u
end

functor F (X : S -> T) (Y : S) = X Y"]]]

      [:section
       [:h1 "シグネチャの束縛"]

       [:p
        "Standard ML 97において、シグネチャの定義はトップレベルでしか許されていませんでしたが、Alice MLではストラクチャ内やシグネチャ内でも可能です。"]

       [:pre
        [:code "structure M : sig
  signature S = sig
    type t
  end
end = struct
  signature S = sig
    type t
  end
end

signature T = M.S
"]]

       [:p
        "最後の行は「ストラクチャ内で定義されたシグネチャには、" [:code "M.S"]
        "のようにアクセスできる」ことを示しています。"
        "(ちなみに、" [:a {:href "https://mosml.org"} "Moscow ML"] "でもストラクチャ内でのシグネチャ定義が可能ですが、ストラクチャ内のシグネチャへアクセスする手段が一切無いせいで非常に不便です。)"]]

      [:section
       [:h1 "抽象シグネチャ"]

       [:p
        "Alice MLでは抽象シグネチャがサポートされています。"
        "これにより、多相的なファンクタを定義できるようになります。"
        "次のコードでは、多相的な恒等ファンクタ" [:code "Id"] "を定義しています。"
        ]

       [:pre
        [:code "functor Id (X : sig
  signature S
  structure M : S
end) : X.S = X.M"]]

       [:p
        "抽象シグネチャの存在は、Alice MLの型検査が"
        [:a {:href "https://elpinal.gitlab.io/writings/ocaml-undecidable-typechecking/"}
         "OCamlと同様の理由"]
        "で、決定不能であることを示します。"
        "OCamlの型検査器を実際に無限ループに突入させるコードを書いたRossberg本人が、Alice MLのモジュールシステムの設計を担当していたと見られるので、意図的に抽象シグネチャを導入しているのだと思います:"]

       [:blockquote
        "We do not consider this a problem in practice, since already the simplest program to make the type checker loop is highly artificial"
        [:footer "— " [:cite [:a {:href "https://www.ps.uni-saarland.de/alice/manual/modules.html#sigmembers"} "Alice Manual - Extensions to the Module Language: Signature members"]]]]]

      [:section
       [:h1 "遅延評価、非同期性"]

       [:p
        "Alice MLでは、コア言語レベルで遅延評価と平行性をサポートしていますが、モジュールにも同様の機能があります。"
        "遅延評価は、モジュールの前に" [:code "lazy"] "キーワードを前置することで実現できます。"]

       [:pre [:code "structure M = lazy struct
  val () = print \"lazy\"
  val x = 4
end"]]

       [:p
        "このモジュールを実行しても、何も出力されません。"
        "次のように、モジュールの中身が必要になったときに始めて\"lazy\"という文字列が出力されます:"]

       [:pre [:code "val _ = M.x + 1"]]

       [:p
        "一方、非同期処理は、" [:code "spawn"] "キーワードによって行なわれます。"
        "次のコードはおそらく\"spawn\"という文字列を出力するでしょう。"]

       [:pre [:code "structure M = spawn struct
  val () = OS.Process.sleep (Time.fromSeconds (LargeInt.fromInt 1))
  val () = print \"wn\"
end

val () = print \"spa\""]]]

      [:section
       [:h1 "First-class packaged modules"]

       [:p
        "Alice MLというのは、Standard MLをベースにしているので、基本的には静的型付き言語です。"
        "しかし、動的検査の利点も活用したい、という訳で"
        [:em "パッケージ"]
        "という仕組みが入っています。"
        "パッケージは" [:code "package"] "という抽象的な型を持つ値であり、"
        "何らかのモジュールを包んでいます。"
        "パッケージからモジュールを取り出すには、明示的な" [:code "unpack"]
        "によって行なわれます。"
        "このとき、動的検査が発生します。"
        "もしパッケージが内包するモジュールのシグネチャが、期待するシグネチャにマッチしない場合、動的にエラーが検出されます。"
        "このようにパッケージは、モジュールを1級の値に変換してコア言語で操作することと、動的検査を実現します。"]

       [:p
        "パッケージを作るには、" [:code "pack M : S"] "という形の式を使います。"
        "この式はモジュール" [:code "M"] "をパッケージに変換します。"
        [:code "M"] "がシグネチャ" [:code "S"] "にマッチすることは、静的に検査されます。"]

       [:pre [:code "structure M = struct
  type t = int

  val z = 0
  fun f x = x
end

signature S = sig
  type t

  val z : t
  val f : t -> int
end

fun f x =
let
  structure N = unpack x : S
in
  N.f N.z
end

val _ : int = f (pack M : S)"]]

       [:p
        [:code "unpack x : S"] "という形の式は、パッケージ" [:code "x"]
        "から、" [:code "S"] "をシグネチャとして持つモジュールを取り出します。"
        [:code "x"] "が内包するモジュールのシグネチャが" [:code "S"] "にマッチすることは動的に検査され、もしマッチしない場合は、実行時例外が発生します。"
        "もしマッチした場合は、" [:code "unpack x : S"] "を「シグネチャ"
        [:code "S"] "を持つモジュール」として扱うことができます。"]

       [:p
        [:code "unpack"] "に対して付けるシグネチャ注釈は、"
        [:code "unpack"] "結果のモジュールに対して知ることのできる唯一の情報です。"
        "したがって先程のコードの、" [:code "unpack"] "を利用する関数を次のように変更すると、型検査を通過しなくなります。"]

       [:pre [:code "fun f x =
let
  structure N = unpack x : S
in
  N.f M.z
end"]]

       [:p
        [:code "M.z"] "は" [:code "M.t"] "型、すなわち" [:code "int"] "型を持ちます。"
        "一方、" [:code "N.f"] "は" [:code "N.t -> int"] "型を持ちます。"
        [:code "N.t"] "型が" [:code "int"] "型と等しいという情報は、" [:code "S"]
        "内に表現されていないので、この関数適用を型付けることはできません。"
        "次のように、シグネチャ注釈をより具体的なものにすることで解決します。"]

       [:pre [:code "fun f x =
let
  structure N = unpack x : S where type t = int
in
  N.f M.z
end"]]

       [:p
        "また、" [:code "unpack"] "での動的検査は" [:code "Package.Mismatch"] "という例外を発生させ得るので、通常の例外処理が行なえます:"]

       [:pre [:code "fun f x =
let
  structure N = unpack x : S
in
  N.f N.z
end handle Package.Mismatch _ => 10"
              ]]

       [:section
        [:h1 "静的検査されるfirst-class packaged moduleとの差異"]

        [:p
         "明示的にコア言語の1級の値に変換されたモジュールをfirst-class packaged moduleと呼びます。"
         "First-class packaged moduleを最初に実装した言語はMoscow MLであり、"
         [:code "unpack"] "は静的に検査されます。"
         "OCamlのfirst-class packaged moduleもMoscow MLと基本的に同じなので、静的に検査されます。"]

        [:p
         "Moscow MLやOCamlと比べて、Alice MLでは全てのモジュールを1つの"
         [:code "package"] "型を持つパッケージに変換できます。"
         "Alice MLにおいて、次のような全く異なるシグネチャを持つ2つのモジュールを、"
         "パッケージに変換して1つのリストに入れることができます。"
         "これは、どのパッケージも" [:code "package"] "という唯一の型を与えられるからです。"]

        [:pre [:code "[ pack struct val a = 1       end
     : sig    val a : int     end
, pack struct type b = string end
     : sig    type b          end
]"]]

        [:p
         "一方、Moscow MLやOCamlではそのようなことはできません。"
         "異なるシグネチャを持つモジュールからは、異なる型を持つパッケージが得られるからです。"
         "そのようにしないと、静的に型検査をすることができません。"
         "Moscow MLにおいて、次のコードは先程のリストの第1要素と第2要素がどのような型を持つかを示しています。"]

        [:pre [:code "val p1 : [sig val a : int end] =
  [structure struct val a = 1 end as sig val a : int end]

val p2 : [sig type b end] =
  [structure struct type b = string end as sig type b end]"]]]

       [:p
        "Moscow MLにおける" [:code "[structure M as S]"]
        "はAlice MLにおける" [:code "pack (M :> S) : S"] "に対応します。"
        "一方、" [:code "[S]"] "はシグネチャ" [:code "S"] "のモジュールから得られるパッケージが持つ型を表します。"
        "ここで、" [:code "p1"] "と" [:code "p2"] "は異なる型を持つので、"
        "Alice MLのように1つのリストに入れることができないのです。"]

       [:p
        "また、静的検査されるパッケージは、部分型を適用できないので不便です。"
        "たとえば、"[:code "f"] "を" [:code "MONOID"] "シグネチャを持つパッケージを受け取る関数とし、"
        [:code "p_string"] "を" [:code "STRING"] "シグネチャを持つパッケージとします。"
        [:code "STRING"] "を" [:code "MONOID"] "の部分シグネチャ、つまりより具体的なシグネチャとします。"
        "Moscow MLでは" [:code "f"] "を" [:code "p_string"] "に適用するために、"
        "一度" [:code "unpack"] "してまた" [:code "pack"] "し直さないといけません"
        "(ここで、Moscow MLの" [:code "structure M as S = x"]
        "はAlice MLの" [:code "structure M = unpack x : S"] " に対応します):"]

       [:pre [:code "let
  structure M as STRING = p_string
in
  f [structure M as MONOID where type t = string]
end"]]

       [:p
        "一方、Alice MLでは単に" [:code "f p_string"] "とすればよいので便利です。"]]

      [:section
       [:h1 "参考文献"]

       [:ol {:class "square"}
        [:li {:id "sml97"} "Robin Milner, Mads Tofte, Robert Harper and David MacQueen. " [:a {:href sml97} [:i "The Definition of Standard ML (Revised)"]] ". MIT Press, 1997."]
        ]]
      ])))

