package main

import (
	"log"
	"net/http"
)

const port = ":8000"

func main() {
	http.Handle("/writings/", http.StripPrefix("/writings/", http.FileServer(http.Dir("./_build"))))

	log.Printf("http://localhost%s/writings/", port)
	log.Fatal(http.ListenAndServe(port, nil))
}
